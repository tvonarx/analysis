\sect{Folgen}

Eine Folge ist eine Abbildung $a: \mathbb{N} \rightarrow \mathbb{R}$. Wir schreiben $a_n$ statt $a(n)$ und bezeichnen eine Folge mit $a(n)_{n\geq1}$

\ssect{Wichtige Folgen}
$a_n = \frac{1}{n} \hfill lim_{n\to\infty} a_n = 0$

$a_n = x^n,\;|x|<1 \hfill lim_{n\to\infty} a_n = 0$


$a_n = \frac{log_x{n}}{n},\; x \in \mathbb{R},\;x>1 \hfill lim_{n\to\infty} a_n = 0$

$a_n = \frac{n^k}{x^n},\; x \in \mathbb{R},\; x > 1 \hfill lim_{n\to\infty} a_n = 0$

$a_n = \frac{x^n}{n!} \hfill lim_{n\to\infty} a_n = 0$

$a_n = \frac{\ln n}{n^x},\;x\in\mathbb{R}^{*+} \hfill lim_{n\to\infty} a_n = 0$


$a_n = \sqrt[n]{x},\; x \in \mathbb{R}^{*+} \hfill lim_{n\to\infty} a_n = 1$

$a_n = \sqrt[n]{n^x},\; x \in \mathbb{R}^{*+} \hfill lim_{n\to\infty} a_n = 1$

$a_n = \sqrt[n]{n} \hfill lim_{n\to\infty} a_n = 1$

$a_n = cos(\frac{1}{n}) \hfill lim_{n\to\infty} a_n = 1$

$a_n = \frac{\sin n}{n} \hfill lim_{n\to0} a_n = 1$

$a_n = \frac{e^x-1}{x} \hfill \lim_{n\to0} a_n = 1$

$a_n = (1+\frac{x}{n})^n \hfill lim_{n\to\infty} a_n = e^x$

$a_n = (1-\frac{x}{n})^n \hfill lim_{n\to\infty} a_n = e^{-x}$

\sssect{Nullfolge}
$a_n$ ist eine \textbf{Nullfolge} iff $\lim_{n\to\infty}a_n = 0$


\ssect{Konvergenz}
Eine Folge konvergiert gegen einen \textbf{Grenzwert} l, falls gilt:
\medskip

\textbf{Def 1}: \blue{Alle Folgenglieder sind irgendwann unendlich nah am Grenzwert} $\forall \epsilon > 0,\exists N \geq 1$, so dass $\forall n \geq N :\; |a_n - l| < \epsilon$

\medskip

\textbf{Def 2}: \blue{Egal wie klein der Bereich um den Grenzwert ist, ausserhalb liegen endlich viele} $\forall \epsilon > 0, \exists N \geq 1$, so dass die Menge ${a_n \notin ]l-\epsilon,l+\epsilon[}$ endlich ist.

\medskip

\textbf{Def Cauchy}: \blue{Die Folgenglieder kommen sich unendlich nahe. Diese Definition verwendet den Grenzwert nicht} $\forall \epsilon > 0 ,\exists N \geq 1$, so dass $\forall n,m \geq N:\; |a_n - a_m| < \epsilon$ 

\sssect{Häufungspunkt}
$\forall \epsilon > 0,\forall N \geq 1,\exists n \geq N:\; |a_n - l|<\epsilon$

\blue{Es gibt unendlich viele Folgenglieder, welche unendlich nahe am Grenzwert liegen. GW $\implies$ HP}

\sssect{Rechnenregeln konvergenter Folgen}

Seien $(a_n)$ und $(b_n)$ konvergente Folgen mit $lim_{n\to\infty}a_n = a$, resp. $lim_{n\to\infty}b_n=b$. Es gilt\medskip

$a_n+b_n\hfill lim_{n\to\infty} a+b$

$a_n\cdot b_n \hfill lim_{n\to\infty} a \cdot b$

$\forall n$ mit $b_n \neq 0:\; \frac{a_n}{b_n} \hfill \lim_{n\to\infty} \frac{a}{b}$

$\exists k \geq 1, \forall n \geq k : \; a_n \leq b_n \implies a \leq b$

\sssect{Sandwichsatz}
\blue{Wenn eine Folge zwischen zwei konvergierenden Folgen mit demselben Grenzwert liegt, dann muss sie auch gegen diesen Grenzwert konvergieren.}
Seien $a_n, b_n$ zwei Folgen mit $\lim_{n\to\infty}a_n = \lim_{n\to\infty}b_n = x$ und $a_n \leq b_n$ für fast alle n. Nun konvergiert jede Folge $c_n$, welche $a_n \leq c_n \leq b_n$ für fast alle n erfüllt, gegen $x$


\ssect{Monotonie}

$(a_n)_{n\geq 1}$ ist \textbf{monoton wachsend} falls $\forall n \geq 1 :\; a_n \leq a_{n+1}$.
Wenn zudem $a_n < a_{n+1}$ gilt, dann ist die Folge \textit{strikt} monoton wachsend. 

$(a_n)_{n\geq 1}$ ist \textbf{monoton fallend} falls $\forall n \geq 1 :\; a_n \geq a_{n+1}$.
Wenn zudem $a_n > a_{n+1}$ gilt, dann ist die Folge \textit{strikt} monoton fallend. 

\ssect{Satz von Weierstrass}
\blue{Falls eine Folge monoton und beschränkt ist, konvergiert sie gegen sup/inf}
\medskip

Sei $(a_n)_{n\geq 1}$ monoton wachsend und nach oben beschränkt. Dann konvergiert $(a_n)_{n\geq 1}$ mit $lim_{n\to\infty}a_n = \sup\{a_n:\;n\geq 1\}$

Analog, sei $(a_n)_{n\geq 1}$ monoton fallend und nach unten beschränkt. Dann konvergiert $(a_n)_{n\geq 1}$ mit $lim_{n\to\infty}a_n = \inf\{a_n:\;n\geq 1\}$

\ssect{Bernoulli Ungleichung}

$\forall n \in\mathbb{N},\; x > -1:\; (1+x)^n \geq 1 + nx$

\ssect{Bolzano-Weierstrass}

\sssect{Teilfolge}
\blue{Eine Teilfolge ist einfach eine Teilmenge einer Folge, wobei die Reihenfolge unverändert bleibt. Jede Teilfolge ist auch eine Folge, sprich beinhaltet unendlich viele Folgenglieder}
\medskip

Formal ist eine \textbf{Teilfolge} einer Folge $(a_n)_{n\geq 1}$ eine Folge $(b_n)_{n\geq 1}$ mit $b_n = a_{l(n)}$ wobei $l: \mathbb{N} \rightarrow \mathbb{N}$ eine Abbildung bezeichnet mit der Eigenschaft $l(n) < l(n+1),\;\forall n \geq 1$.
Der Grenzwert einer Teilfolge heisst \textbf{Häufungspunkt}

\medskip
\sssect{Satz über konvergente Folgen}
- Jede beschränkte Folge besitzt mindestens eine konvergente Teilfolge

- Jede Teilfolge einer konvergenten Folge konvergiert

\ssect{Limes superior und Limes inferior}

Mit jeder beschränkten Folge lassen sich zwei monotone Folgen $(b_n)_{n \geq 1}$ und $(c_n)_{n \geq 1}$ definieren, welche dann einen Grenzwert besitzen. Sei $\forall n \geq 1:\;$ 

$b_n = \inf\{a_k:\;k \geq n\}$ \hfill \blue{monoton wachsend}

$c_n = \sup\{a_k:\;k \geq n\}$ \hfill \blue{monoton fallend}

\medskip
Anschaulich:

$b_1 = \inf\{a_1, a_2, a_3,...\}$
$c_1 = \sup\{a_1, a_2, a_3,...\}$

$b_2 = \inf\{a_2, a_3, a_4,...\}$
$c_2 = \sup\{a_2, a_3, a_4,...\}$

$b_3 = \inf\{a_3, a_4, a_5,...\}$
$c_3 = \sup\{a_3, a_4, a_5,...\}$

\medskip

$\lim_{n\to\infty} \inf a_n := lim_{n\to\infty} b_n$ ist der \textbf{Limes inferior} \blue{$:=$ Der kleinste Häufungspunkt}

$\lim_{n\to\infty} \sup a_n := lim_{n\to\infty} c_n$ ist der \textbf{Limes superior} \blue{$:=$ Der grösste Häufungspunkt}

\medskip

Es gilt $\lim_{n\to\infty} \inf a_n = lim_{n\to\infty} \sup a_n \implies a_n$ ist konvergent

\ssect{Abgeschlossenes Intervall}

Ein abgeschlossenes Intervall ist in sich geschlossen, sprich "lückenfrei". Deshalb kann man eine Länge $\mathcal{L}$ bestimmen. Es gilt $\mathcal{L}(I_a) = b-a$

\sssect{Cauchy-Cantor}

Sei $I_1 \supseteq I_2 \supseteq I_3 \supseteq ... \supseteq I_n \supseteq I_{n+1} \supseteq ...$ eine Folge abgeschlossener Intervalle mit $\mathcal{L}(I_1) < +\infty$
\blue{Betrachtet man die Längen dieser Intervalle, dann ergeben sie eine monoton fallende Folge}

Es gilt: $\cap_{n\geq 1} I_n \neq \emptyset$, \blue{die Schnittmenge der ganzen Intervalle ist nicht leer, weil immer weitere Intervalle mit unendlich kleiner Länge folgen}

Ist $\lim_{n\to\infty}\mathcal{L}_{I_n} = 0$, enthält die Schnittmenge genau einen Punkt

\ssect{Rekursive Folgen}
- Folgenglieder ausrechnen um Tendenz festzustellen

- Mit Induktion Monotonie beweisen

- obere resp. untere Schranke finden und beweisen mit Induktion

- Nach Weierstrass folgt die Konvergenz

- Alternativ, umformen in eine direkte Formel

\sssect{Grenzwert finden "Induktionstrick"}

$lim_{n\to\infty}a_n = a \implies$ für jede Teilfolge $l(n)$ gilt $lim_{n\to\infty}a_{l(n)} = d$

\medskip

Sei $l(n) := n+1$, dann gilt $\lim_{n\to\infty}a_{n+1} = a = \lim_{n\to\infty}a_n$

\medskip

Mit $a = \lim_{n\to\infty a_{n+1}}$ anfangen und dann den Wert von $a$ erhalten durch Umformen













