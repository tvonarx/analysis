zf:
	pdflatex zf.tex
	xdg-open zf.pdf

clean:
	git clean -dfx
	rm -f *.{ps,pdf,log,aux,out,dvi,bbl,blg}
